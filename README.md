

# Sujet 10 : Objets et classes : combat d'orques

# Introduction

Nous allons développer un programme de jeu qui consiste à gérer des
**orques** qui s'affrontent dans des arènes.

Nous proposons plusieurs versions du jeu de plus en plus élaborées, en
donnant des pistes d'extension. N'hésitez-pas à partir sur d'autres
choix.

# Préambule

En plus des classes codant le jeu, nous utiliserons la classe ` Ut`.
Vous devrez également coder une classe `EO` (ensemble d'orques), une
variante de la classe `EE` qui gère des orques à la place des entiers.

Codez et tester la classe `EO` qui contient essentiellement des méthodes
`ajoutElt`, `ajoutPratique` et ` retraitPratique`, ainsi que les
méthodes suivantes qui seront utiles.

    /** Pré-requis :      ensemble this est non vide
     *  Résultat/action : enlève un élément de this (aléatoirement) et le renvoie
     */
    public Orque retraitEltAleatoirement() {
       int i = Ut.randomMinMax (0, this.cardinal - 1);
       Orque select = retraitPratique(i);
       return select;
    }

    /** Pré-requis : ensemble this est non vide
     *  Résultat :   un élément quelconque de this choisi aléatoirement
     */
    public Orque selectionEltAleatoirement() {
         int i = Ut.randomMinMax (0, this.cardinal - 1);
         return this.ensTab[i];
    }

    /** Pré-requis : ensemble this est non vide
     *  Résultat :   un élément quelconque de this 
     */
    public Orque selectionElt() {
        return this.ensTab[this.cardinal - 1];
    }

Vous pouvez supprimer `selectElt()`. et `selectEltAleatoirement()` pour éviter les confusions . 

### Remarque 

N'est-ce pas dommage de devoir réécrire cette classe `EO` en remplaçant
un peu partout dans le code le type `int` par le type `Orque` ? C'est
sale. Vous étudierez au second semestre le principe de généricité qui
permettra d'obtenir un code unique (générique) pouvant prendre en compte
n'importe quel type d'éléments.

# Version de base

Le jeu comprend trois classes principales : `Orque`, `Arene`,
`MainCombat`.

## La classe Orque {}

Dans sa version de base, la classe `Orque` comprend deux variables
d'instance (attributs) : un numéro d'identification `id` et un `score`
entier qui indique le nombre de duels gagnés.

La classe comprend également deux *variables de classe* :

-   un entier `nextId` donnant le prochain numméro d'orque créé
    dans la partie. Cette variable permettra d'attribuer automatiquement
    un identifiant unique à chacun des orques créés.

-   un attribut `legende` qui est (une référence sur) un objet de
    type Orque : `legende` référence l'orque tué au combat qui a gagné
    le plus de duels jusqu'à présent.

Il faut coder une méthode de construction qui initialise les variables
d'instance et met à jour les variables de classe.

Il faut aussi prévoir une méthode `duel` de combat avec un autre orque
retournant l'orque gagnant. Dans cette version naïve, le gagnant sera
choisi au hasard.

### Remarques, indications 

Les variables de classe (statiques) sont initialisées au chargement de
la *classe* (par la JVM) et ne sont donc pas initialisées dans les
constructeurs (d'objets).

La méthode de classe `Ut.randomMinMax(int min, int max)` permet de
renvoyer un nombre entier pseudo-aléatoire compris entre `min` et `max`
(lire le code dans `Ut.java`).

## La classe Arene 

Dans cette première version, la classe `Arene` comprend essentiellement
comme attribut un ensemble d'orques `ensOrques` (de type `EO`) contenant
les orques encore vivants de l'arène.

Le constructeur de `Arene` prend en paramètre un nombre d'orques `nbo`.
Il crée (construit) `nbo` orques qui combattront dans cette arène.

La méthode `bataille` gère les combats entre les orques de l'ensemble
`this.ensOrques`. Elle procède itérativement à des duels à mort entre
deux orques jusqu'à ce qu'il n'en reste plus qu'un. Pour un duel donné,
deux éléments de `this.ensOrques` sont sélectionnés au hasard et retirés
de l'ensemble. Après le duel, le gagnant est remis dans
`this.ensOrques`.

## Le programme principal : la classe MainCombat 

La procédure principale construit plusieurs arènes (qui créent
elles-mêmes des ensembles d'orques combattants) et appelle la méthode
` bataille` sur ces arènes. Un affichage permet de connaître l'orque
vainqueur dans chaque arêne, ainsi que la légende.

# Extensions (bonus)

Une fois cette première version codée, on pourra rendre la simulation
plus réaliste en améliorant deux points principaux :

-   améliorer les duels en ajoutant et prenant en compte des
    caractéristiques des orques : poids, points de vie (pdv, 0
    signifiant la mort de l'orque), aggressivité, etc., et un équipement
    en armes.

-   placer les orques spatialement : les orques sont placés sur l'arène
    (qui est un carré de taille de côté donné) en une position donnée
    (abscisse et ordonnée), et se déplacent pour combattre.

## Des duels plus réalistes (bonus)

Les suggestions suivantes sont données à titre indicatif. Ne pas hésiter
à faire d'autres choix.

## Caractéristiques des orques et des armes 

Les caractéristiques des orques peuvent rendre les combats plus
réalistes. On peut les choisir aléatoirement (dans un intervalle donné)
lors de leur création pour disposer d'individus différents. On peut
envisager les attributs `pdv`, un entier qui diminue au cours des
combats (0 signe l'arrêt de mort de l'orque), `poids`, qui modifie les
dégâts causés par l'arme, et `aggressivite` (ou `dexterite`), qui
modifie la chance de toucher l'adversaire. Enfin, l'attribut ` armes` de
type `EE` qui donne l'équipement en armes de l'orque, représenté par
l'ensemble des identifiants des armes (voir ci-dessous).

Un duel entre un orque et son adversaire peut se dérouler comme suit.
Ils prennent chacun une de leurs armes (au hasard). Tant que les deux
adversaires sont en vie, un des duettistes cherche à blesser l'autre, à
tour de rôle. Sa probabilité de toucher l'adversaire dépend des
caractéristiques de son arme et de sa dextérité. En cas de touche, les
dégâts causés dépendent de l'arme et du poids de l'attaquant.

## Une classe Arme {}

On pourra définir une arme par son `type` (une chaîne de caractères
comme "hache", "epee", "lance", "fléau"), les `degats` causés,
c'est-à-dire le nombre de pdv que l'arme peut retirer à l'adversaire à
chaque touche, et un nombre entre 0 et 100 `probaTouche`, une
probabilité de toucher l'adversaire.

On pourra définir une variable de la classe `Arme` nommée ` tabArmes`
qui est un tableau de tous les objets `Arme` existants : un objet pour
l'épée, un autre pour la hache, etc. Noter qu'il n'y a dans le tableau
qu'un exemplaire de chaque type d'armes, si bien qu'en pratique toutes
les épées utilisées par les orques auront les mêmes caractéristiques
(dégâts et probabilité de toucher).

A la création d'un orque, on pourra lui associer un ensemble `armes` de
la classe `EE`, les entiers représentant les indices dans le tableau
`Arme.tabArmes`.

Prévoir une méthode permettant de récupérer l'objet arme (et donc ses
caractéristiques) à partir de son identifiant (l'indice dans le
tableau).

## Placement dans l'arène

On peut aussi considèrer que les orques ont une position $(x,y)$ dans
l'arène et se déplacent.

On augmente le réalisme des combats en faisant évoluer les coordonnées
des orques. Par exemple, tous les orques se déplacent d'une case entre
chaque tour de jeu (aléatoirement), et les orques à une certaine
distance l'un de l'autre s'affrontent en duel.
